const dateToString = (date: Date): string => {
  return date.toLocaleString("en-UK", {
    day: "2-digit",
    month: "short",
    year: "numeric",
  });
};

export default dateToString;
