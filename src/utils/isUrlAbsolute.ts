export const ABSOLUTE_URL_REGEX = /((?:https?:)?\/\/(?:[\w]+[.][\w]+)+\/?)+$/gm;

export const isUrlAbsolute = (url: string) => ABSOLUTE_URL_REGEX.test(url);
