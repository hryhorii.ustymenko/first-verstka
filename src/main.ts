import Vue from "vue";

import App from "./App.vue";
import router from "./router";
import VueFlicking from "@egjs/vue-flicking";

import "normalize.css";
import "./assets/main.scss";
import "@egjs/vue-flicking/dist/flicking.css";

Vue.use(VueFlicking);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
